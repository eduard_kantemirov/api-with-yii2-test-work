<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "book".
 *
 * @property int $id
 * @property string $author
 * @property string $title
 * @property string|null $alias
 */
class BaseBook extends ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'book';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['author', 'title'], 'required'],
            [['author', 'title', 'alias'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'     => 'ID',
            'author' => 'Author',
            'title'  => 'Title',
            'alias'  => 'Alias',
        ];
    }

    /**
     * Gets the book issues for the book.
     */
    public function getBookIssues() {
        return $this->hasMany(BaseBookIssue::class, ['book_id' => 'id']);
    }


}

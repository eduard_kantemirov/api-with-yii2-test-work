<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "book_issue".
 *
 * @property int $id
 * @property int $book_id
 * @property int $reader_id
 * @property string $expected_return_date
 * @property string $issue_date
 * @property string|null $return_date
 *
 * @property BaseBook $book
 * @property BaseReader $reader
 */
class BaseBookIssue extends ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'book_issue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['book_id', 'reader_id'], 'required'],
            [['book_id', 'reader_id'], 'integer'],
            [['expected_return_date', 'issue_date', 'return_date'], 'safe'],
            [['book_id'], 'exist', 'skipOnError' => true, 'targetClass' => BaseBook::class,
             'targetAttribute'                   => ['book_id' => 'id']],
            [['reader_id'], 'exist', 'skipOnError' => true, 'targetClass' => BaseReader::class,
             'targetAttribute'                     => ['reader_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'                   => 'ID',
            'book_id'              => 'Book ID',
            'reader_id'            => 'Reader ID',
            'expected_return_date' => 'Expected Return Date',
            'issue_date'           => 'Issue Date',
            'return_date'          => 'Return Date',
        ];
    }

    /**
     * Gets query for [[Book]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBook() {
        return $this->hasOne(BaseBook::class, ['id' => 'book_id']);
    }

    /**
     * Gets query for [[Reader]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReader() {
        return $this->hasOne(BaseReader::class, ['id' => 'reader_id']);
    }
}

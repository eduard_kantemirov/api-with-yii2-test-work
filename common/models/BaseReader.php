<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "reader".
 *
 * @property int $id
 * @property string $name
 * @property string $registration_date
 */
class BaseReader extends ActiveRecord {
    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return 'reader';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name'], 'required'],
            [['registration_date'], 'safe'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id'                => 'ID',
            'name'              => 'Name',
            'registration_date' => 'Registration Date',
        ];
    }

    /**
     * Gets the book issues for the reader.
     */
    public function getBookIssues() {
        return $this->hasMany(BaseBookIssue::class, ['reader_id' => 'id']);
    }
}

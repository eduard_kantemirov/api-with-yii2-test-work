<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%reader}}`.
 */
class m231111_182456_create_reader_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%reader}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'registration_date' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%reader}}');
    }
}

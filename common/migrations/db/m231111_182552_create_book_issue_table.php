<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%book_issue}}`.
 */
class m231111_182552_create_book_issue_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%book_issue}}', [
            'id' => $this->primaryKey(),
            'book_id' => $this->integer()->notNull(),
            'reader_id' => $this->integer()->notNull(),
            'expected_return_date' => $this->timestamp()->notNull(),
            'issue_date' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'return_date' => $this->timestamp()->null(),
        ]);

        $this->createIndex(
            'idx-book_issue-book_id',
            '{{%book_issue}}',
            'book_id'
        );

        $this->addForeignKey(
            'fk-book_issue-book_id',
            '{{%book_issue}}',
            'book_id',
            '{{%book}}',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-book_issue-reader_id',
            '{{%book_issue}}',
            'reader_id'
        );

        $this->addForeignKey(
            'fk-book_issue-reader_id',
            '{{%book_issue}}',
            'reader_id',
            '{{%reader}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-book_issue-reader_id',
            '{{%book_issue}}'
        );

        $this->dropIndex(
            'idx-book_issue-reader_id',
            '{{%book_issue}}'
        );

        $this->dropForeignKey(
            'fk-book_issue-book_id',
            '{{%book_issue}}'
        );

        $this->dropIndex(
            'idx-book_issue-book_id',
            '{{%book_issue}}'
        );

        $this->dropTable('{{%book_issue}}');
    }
}

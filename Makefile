SHELL=/bin/bash -e

.DEFAULT_GOAL := help
.PHONY: *

-include .env

DC = docker-compose -f docker-compose.yml

help: ## Справка
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

dev-build:
	@cp .env.dist .env
	@${DC} up --detach
	@${DC} exec app bash -c 'composer install --ignore-platform-reqs'
	@${DC} exec app bash -c 'php console/yii app/setup --interactive=0'
	@${DC} exec app bash -c 'cd frontend/web && ln -s ../../backend/web backend && ln -s ../../api/web api && ln -s ../../storage/web storage'
# 	@${DC} run -T --rm webpacker npm run build


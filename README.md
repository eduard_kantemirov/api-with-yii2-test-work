# Интеграция отчетов

## Разворачивание проекта

1. В корне проекта запускаем команду ```make dev-build```

## ручное
cp .env.dist .env  
docker-compose up --build --detach  
docker-compose exec app bash  
composer install --ignore-platform-reqs  
php console/yii app/setup --interactive=0  
cd frontend/web && ln -s ../../backend/web backend && ln -s ../../api/web api && ln -s ../../storage/web storage  
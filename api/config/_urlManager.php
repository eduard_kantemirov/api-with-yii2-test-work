<?php
return [
    'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
        // Api
        ['class' => 'yii\rest\UrlRule', 'controller' => 'v1/article', 'only' => ['index', 'view', 'options']],
        [
            'class' => \yii\rest\UrlRule::class,
            'controller' => [
                'v1/book',
                'v1/reader',
                'v1/book-issue',
            ],
            'extraPatterns' => [
                'OPTIONS <action>' => 'options',
            ],
            'pluralize' => false,
        ],
    ],
];

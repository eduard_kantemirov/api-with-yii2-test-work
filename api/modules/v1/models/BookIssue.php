<?php

namespace api\modules\v1\models;

use common\models\BaseBookIssue;
use Yii;

/**
 * This is the extension of model class for table "book_issue".
 *
 */
class BookIssue extends BaseBookIssue {

}

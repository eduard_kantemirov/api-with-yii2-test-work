<?php

namespace api\modules\v1\models\definitions;
/**
 * @SWG\Definition(required={})
 * @SWG\Property (
 *     property="_items",
 *     type="array",
 *     description="Записи, возвращаемые запросом",
 *     @SWG\Items(ref="#/definitions/Reader")
 * ),
 * @SWG\Property (
 *     property="_links",
 *     type="object",
 *     description="Ссылки",
 *     @SWG\Property(
 *         property="self",
 *         type="object",
 *         @SWG\Property (property="href", type="string")
 *     )
 * ),
 * @SWG\Property (
 *     property="_meta",
 *     type="object",
 *     description="Пагинация",
 *     @SWG\Property(property="totalCount", type="integer"),
 *     @SWG\Property(property="pageCount", type="integer"),
 *     @SWG\Property(property="currentPage", type="integer"),
 *     @SWG\Property(property="perPage", type="integer"),
 * ),
 */
class ReaderCollection {
    // dummy class for Swagger definitions
}
<?php

namespace api\modules\v1\models\definitions;
/**
 * @SWG\Definition(required={})
 * @SWG\Property(property="id", type="integer", description=""),
 * @SWG\Property(property="name", type="string", description=""),
 * @SWG\Property(property="registration_date", type="string", description=""),
 */
class Reader {
    // dummy class for Swagger definitions
}
<?php

namespace api\modules\v1\models\definitions;
/**
 * @SWG\Definition(required={})
 * @SWG\Property(property="id", type="integer", description=""),
 * @SWG\Property(property="author", type="string", description=""),
 * @SWG\Property(property="title", type="string", description=""),
 * @SWG\Property(property="alias", type="string", description=""),
 */
class Book {
    // dummy class for Swagger definitions
}
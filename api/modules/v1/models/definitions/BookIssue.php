<?php

namespace api\modules\v1\models\definitions;
/**
 * @SWG\Definition(required={})
 * @SWG\Property(property="id", type="integer", description=""),
 * @SWG\Property(property="book_id", type="integer", description=""),
 * @SWG\Property(property="reader_id", type="integer", description=""),
 * @SWG\Property(property="expected_return_date", type="string", description=""),
 * @SWG\Property(property="issue_date", type="string", description=""),
 * @SWG\Property(property="return_date", type="string", description=""),
 */
class BookIssue {
    // dummy class for Swagger definitions
}
<?php

namespace api\modules\v1\resources;

use yii\helpers\ArrayHelper;

class Reader extends \api\modules\v1\models\Reader {
    public function fields() {
        return ArrayHelper::merge(parent::fields(), []);


        /*
        return [
            //TODO список полей ресурса
        ];
        */
    }

    public function extraFields() {
        return ['bookIssues'];
    }
}
<?php

namespace api\modules\v1\resources;

use yii\helpers\ArrayHelper;

class BookIssue extends \api\modules\v1\models\BookIssue {
    public function fields() {
        return ArrayHelper::merge(parent::fields(), [
            //TODO дополнительные поля
        ]);

        /*
        return [
            //TODO список полей ресурса
        ];
        */
    }

    public function extraFields() {
        return ['book', 'reader'];
    }
}
<?php

namespace api\modules\v1\resources\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\BookIssue;
use yii\db\ActiveRecord;

/**
 * BookIssueSearch represents the model behind the search form of `api\modules\v1\models\BookIssue`.
 */
class BookIssueSearch extends BookIssue {
    public $modelClass = \api\modules\v1\resources\BookIssue::class;
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        /** @var ActiveRecord $modelClass */
        $modelClass = $this->modelClass;
        $query = $modelClass::find();

        // add conditions that should always apply here
        $query->joinWith([
            'book' => function ($q) use ($params) {
                if (!empty($params['book_title'])) {
                    $q->andWhere(['book.title' => $params['book_title']]);
                }
            },
            'reader' => function ($q) use ($params) {
                if (!empty($params['reader_name'])) {
                    $q->andWhere(['reader.name' => $params['reader_name']]);
                }
            }
        ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($params['per-page']) ? (int) $params['per-page'] : 20,
                'page' => isset($params['page']) ? (int) $params['page'] - 1 : 0,
            ],
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        return $dataProvider;
    }
}

<?php

namespace api\modules\v1\resources\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use api\modules\v1\models\Reader;
use yii\db\ActiveRecord;

/**
 * ReaderSearch represents the model behind the search form of `api\modules\v1\models\Reader`.
 */
class ReaderSearch extends Reader {
    public $modelClass = \api\modules\v1\resources\Reader::class;
    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['name'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        /** @var ActiveRecord $modelClass */
        $modelClass = $this->modelClass;
        $query = $modelClass::find();

        // add conditions that should always apply here
        $query->joinWith(['bookIssues' => function ($query) use ($params) {
            if (!empty($params['expected_return_date'])) {
                $query->andWhere(['book_issue.expected_return_date' => $params['expected_return_date']]);
            }
        }]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => isset($params['per-page']) ? (int) $params['per-page'] : 20,
                'page' => isset($params['page']) ? (int) $params['page'] - 1 : 0,
            ],
        ]);

        $this->load($params, '');

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}

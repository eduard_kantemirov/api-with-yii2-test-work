<?php

namespace api\modules\v1\controllers;

use api\modules\v1\controllers\actions\CloseIssueAction;
use Swagger\Annotations as SWG;
use Yii;
use yii\filters\AccessControl;
use yii\rest\Controller;
use api\modules\v1\resources\BookIssue;
use api\modules\v1\resources\search\BookIssueSearch;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\CreateAction;
use yii\rest\DeleteAction;
use yii\rest\IndexAction;
use yii\rest\UpdateAction;
use yii\rest\ViewAction;

/**
* BookIssueController implements the CRUD actions for BookIssue model.
*/
class BookIssueController extends BaseController {
    public $modelClass = BookIssue::class;
    public $serializer = [
        'class' => \yii\rest\Serializer::class,
        'collectionEnvelope' => '_items',
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'list' => ['get'],
                    'view' => ['get'],
                    'create' => ['post'],
                    'update' => ['patch', 'put'],
                    'delete' => ['delete'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'except' => ['options'],
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['create', 'update', 'list', 'view', 'delete', 'close'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function actions(): array {
        $parentActions = parent::actions();
        $actions = [
            /**
             * @SWG\Post(path="/api/v1/book-issue/create",
             *     tags={"Журнал выдачи книг"},
             *     summary="Создать",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="book_id",
             *         description="",
             *         type="integer",
             *         in="formData",
             *         required=true,
             *     ),
             *     @SWG\Parameter(
             *         name="reader_id",
             *         description="",
             *         type="integer",
             *         in="formData",
             *         required=true,
             *     ),
             *     @SWG\Parameter(
             *         name="expected_return_date",
             *         description="",
             *         type="string",
             *         in="formData",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="issue_date",
             *         description="",
             *         type="string",
             *         in="formData",
             *         required=false,
             *     ),
             *     @SWG\Response(
             *         response = 201,
             *         description="Объект созданной записи",
             *         @SWG\Schema(ref = "#/definitions/BookIssue")
             *     )
             * ),
             */
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Post(path="/api/v1/book-issue/close",
             *     tags={"Журнал выдачи книг"},
             *     summary="Закрыть",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="id",
             *         description="",
             *         type="integer",
             *         in="query",
             *         required=true
             *     ),
             *     @SWG\Response(
             *         response = 204,
             *         description = "Запись закрыта",
             *     ),
             * )
             */
            'close' => [
                'class' => CloseIssueAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Get(path="/api/v1/book-issue/view",
             *     tags={"Журнал выдачи книг"},
             *     summary="Просмотр записи",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="id",
             *         description="",
             *         type="integer",
             *         in="query",
             *         required=true
             *     ),
             *     @SWG\Response(
             *         response = 200,
             *         description = "Объект записи",
             *         @SWG\Schema(ref = "#/definitions/BookIssue")
             *     )
             * )
             */
            'view' => [
                'class' => ViewAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Get(path="/api/v1/book-issue/list",
             *     tags={"Журнал выдачи книг"},
             *     summary="Список записей",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="book_title",
             *         description="",
             *         type="string",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="reader_name",
             *         description="",
             *         type="string",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="page",
             *         description="Страница",
             *         type="integer",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="per-page",
             *         description="Объектов на странице",
             *         type="integer",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="sort",
             *         description="Сортировка столбцов",
             *         type="string",
             *         enum={"id", "-id", "book_id", "-book_id", "reader_id", "-reader_id", "expected_return_date", "-expected_return_date", "issue_date", "-issue_date", "return_date", "-return_date"},
             *         in="query"
             *     ),
             *     @SWG\Response(
             *         response = 200,
             *         description = "Список объектов",
             *         @SWG\Schema(ref = "#/definitions/BookIssueCollection")
             *     )
             * )
             */
            'list' => [
                'class' => IndexAction::class,
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function ($action) {
                    $searchModel = new BookIssueSearch();
                    return $searchModel->search(Yii::$app->request->queryParams);
                },
            ],
        ];
        return ArrayHelper::merge($parentActions, $actions);
    }
}

<?php

namespace api\modules\v1\controllers\actions;

use api\modules\v1\models\BookIssue;
use Yii;
use yii\rest\Action;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class CloseIssueAction extends Action {

    /**
     * @throws NotFoundHttpException
     * @throws ServerErrorHttpException
     */
    public function run($id) {
        /* @var $model BookIssue */
        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        $model->return_date = (new \DateTime())->format('Y-m-d H:i:s');
        $model->save();

        if ($model->save()) {
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to close issue for unknown reason.');
        }

        return $model;
    }
}

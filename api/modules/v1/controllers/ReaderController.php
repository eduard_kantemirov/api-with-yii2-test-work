<?php

namespace api\modules\v1\controllers;

use Swagger\Annotations as SWG;
use Yii;
use yii\filters\AccessControl;
use yii\rest\Controller;
use api\modules\v1\resources\Reader;
use api\modules\v1\resources\search\ReaderSearch;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\CreateAction;
use yii\rest\DeleteAction;
use yii\rest\IndexAction;
use yii\rest\Serializer;
use yii\rest\UpdateAction;
use yii\rest\ViewAction;

/**
* ReaderController implements the CRUD actions for Reader model.
*/
class ReaderController extends BaseController {
    public $modelClass = Reader::class;
    public        $serializer = [
        'class' => Serializer::class,
        'collectionEnvelope' => '_items',
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'list'   => ['get'],
                    'view'   => ['get'],
                    'create' => ['post'],
                    'update' => ['patch', 'put'],
                    'delete' => ['delete'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'except' => ['options'],
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['create', 'update', 'list', 'view', 'delete'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function actions(): array {
        $parentActions = parent::actions();
        $actions = [
            /**
             * @SWG\Post(path="/api/v1/reader/create",
             *     tags={"Читатели"},
             *     summary="Создать",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="name",
             *         description="",
             *         type="string",
             *         in="formData",
             *         required=true,
             *     ),
             *     @SWG\Parameter(
             *         name="registration_date",
             *         description="",
             *         type="string",
             *         in="formData",
             *         required=false,
             *     ),
             *     @SWG\Response(
             *         response = 201,
             *         description="Объект созданной записи",
             *         @SWG\Schema(ref = "#/definitions/Reader")
             *     )
             * ),
             */
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Get(path="/api/v1/reader/view",
             *     tags={"Читатели"},
             *     summary="Просмотр записи",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="id",
             *         description="",
             *         type="integer",
             *         in="query",
             *         required=true
             *     ),
             *     @SWG\Response(
             *         response = 200,
             *         description = "Объект записи",
             *         @SWG\Schema(ref = "#/definitions/Reader")
             *     )
             * )
             */
            'view' => [
                'class' => ViewAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Get(path="/api/v1/reader/list",
             *     tags={"Читатели"},
             *     summary="Список записей",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="name",
             *         description="",
             *         type="string",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="expected_return_date",
             *         description="",
             *         type="string",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="page",
             *         description="Страница",
             *         type="integer",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="per-page",
             *         description="Объектов на странице",
             *         type="integer",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="sort",
             *         description="Сортировка столбцов",
             *         type="string",
             *         enum={"id", "-id", "name", "-name", "registration_date", "-registration_date"},
             *         in="query"
             *     ),
             *     @SWG\Response(
             *         response = 200,
             *         description = "Список объектов",
             *         @SWG\Schema(ref = "#/definitions/ReaderCollection")
             *     )
             * )
             */
            'list' => [
                'class' => IndexAction::class,
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function ($action) {
                    $searchModel = new ReaderSearch();
                    return $searchModel->search(Yii::$app->request->queryParams);
                },
            ],
            /**
             * @SWG\Put(path="/api/v1/reader/update",
             *     tags={"Читатели"},
             *     summary="Редактировать",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="id",
             *         description="",
             *         type="integer",
             *         in="query",
             *         required=true
             *     ),
             *     @SWG\Parameter(
             *         name="name",
             *         description="",
             *         type="string",
             *         in="formData"
             *     ),
             *     @SWG\Parameter(
             *         name="registration_date",
             *         description="",
             *         type="string",
             *         in="formData"
             *     ),
             *     @SWG\Response(
             *         response = 200,
             *         description="Объект обновлённой записи",
             *         @SWG\Schema(ref = "#/definitions/Reader")
             *     )
             * )
             */
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Delete(path="/api/v1/reader/delete",
             *     tags={"Читатели"},
             *     summary="Удалить",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="id",
             *         description="",
             *         type="integer",
             *         in="query",
             *         required=true
             *     ),
             *     @SWG\Response(
             *         response = 204,
             *         description = "Запись удалена",
             *     ),
             * )
             */
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => $this->modelClass,
            ],
        ];
        return ArrayHelper::merge($parentActions, $actions);
    }
}

<?php

namespace api\modules\v1\controllers;

use Swagger\Annotations as SWG;
use Yii;
use yii\filters\AccessControl;
use yii\rest\Controller;
use api\modules\v1\resources\Book;
use api\modules\v1\resources\search\BookSearch;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\rest\CreateAction;
use yii\rest\DeleteAction;
use yii\rest\IndexAction;
use yii\rest\UpdateAction;
use yii\rest\ViewAction;

/**
* BookController implements the CRUD actions for Book model.
*/
class BookController extends BaseController {
    public $modelClass = Book::class;
    public $serializer = [
        'class' => \yii\rest\Serializer::class,
        'collectionEnvelope' => '_items',
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors(): array {
        return ArrayHelper::merge(parent::behaviors(), [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'list' => ['get'],
                    'view' => ['get'],
                    'create' => ['post'],
                    'update' => ['patch', 'put'],
                    'delete' => ['delete'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'except' => ['options'],
                'rules' => [
                    [
                        'allow'   => true,
                        'actions' => ['create', 'update', 'list', 'view', 'delete'],
                        'roles'   => ['@'],
                    ],
                ],
            ],
        ]);
    }

    public function actions(): array {
        $parentActions = parent::actions();
        $actions = [
            /**
             * @SWG\Post(path="/api/v1/book/create",
             *     tags={"Книги"},
             *     summary="Создать",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="author",
             *         description="",
             *         type="string",
             *         in="formData",
             *         required=true,
             *     ),
             *     @SWG\Parameter(
             *         name="title",
             *         description="",
             *         type="string",
             *         in="formData",
             *         required=true,
             *     ),
             *     @SWG\Parameter(
             *         name="alias",
             *         description="",
             *         type="string",
             *         in="formData",
             *         required=false,
             *     ),
             *     @SWG\Response(
             *         response = 201,
             *         description="Объект созданной записи",
             *         @SWG\Schema(ref = "#/definitions/Book")
             *     )
             * ),
             */
            'create' => [
                'class' => CreateAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Get(path="/api/v1/book/view",
             *     tags={"Книги"},
             *     summary="Просмотр записи",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="id",
             *         description="",
             *         type="integer",
             *         in="query",
             *         required=true
             *     ),
             *     @SWG\Response(
             *         response = 200,
             *         description = "Объект записи",
             *         @SWG\Schema(ref = "#/definitions/Book")
             *     )
             * )
             */
            'view' => [
                'class' => ViewAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Get(path="/api/v1/book/list",
             *     tags={"Книги"},
             *     summary="Список записей",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="alias",
             *         description="",
             *         type="string",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="issue_date",
             *         description="",
             *         type="string",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="expected_return_date",
             *         description="",
             *         type="string",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="page",
             *         description="Страница",
             *         type="integer",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="per-page",
             *         description="Объектов на странице",
             *         type="integer",
             *         in="query",
             *         required=false,
             *     ),
             *     @SWG\Parameter(
             *         name="sort",
             *         description="Сортировка столбцов",
             *         type="string",
             *         enum={"id", "-id", "author", "-author", "title", "-title", "alias", "-alias"},
             *         in="query"
             *     ),
             *     @SWG\Response(
             *         response = 200,
             *         description = "Список объектов",
             *         @SWG\Schema(ref = "#/definitions/BookCollection")
             *     )
             * )
             */
            'list' => [
                'class' => IndexAction::class,
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function ($action) {
                    $searchModel = new BookSearch();
                    return $searchModel->search(Yii::$app->request->queryParams);
                },
            ],
            /**
             * @SWG\Patch(path="/api/v1/book/update",
             *     tags={"Книги"},
             *     summary="Редактировать",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="id",
             *         description="",
             *         type="integer",
             *         in="query",
             *         required=true
             *     ),
             *     @SWG\Parameter(
             *         name="author",
             *         description="",
             *         type="string",
             *         in="formData"
             *     ),
             *     @SWG\Parameter(
             *         name="title",
             *         description="",
             *         type="string",
             *         in="formData"
             *     ),
             *     @SWG\Parameter(
             *         name="alias",
             *         description="",
             *         type="string",
             *         in="formData"
             *     ),
             *     @SWG\Response(
             *         response = 200,
             *         description="Объект обновлённой записи",
             *         @SWG\Schema(ref = "#/definitions/Book")
             *     )
             * )
             */
            'update' => [
                'class' => UpdateAction::class,
                'modelClass' => $this->modelClass,
            ],
            /**
             * @SWG\Delete(path="/api/v1/book/delete",
             *     tags={"Книги"},
             *     summary="Удалить",
             *     security={{"Bearer": {}}},
             *     @SWG\Parameter(
             *         name="id",
             *         description="",
             *         type="integer",
             *         in="query",
             *         required=true
             *     ),
             *     @SWG\Response(
             *         response = 204,
             *         description = "Запись удалена",
             *     ),
             * )
             */
            'delete' => [
                'class' => DeleteAction::class,
                'modelClass' => $this->modelClass,
            ],
        ];
        return ArrayHelper::merge($parentActions, $actions);
    }
}
